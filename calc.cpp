#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "tree.h"

const char latexFileName[] = "diff.tex";

#define SUM_(val, val2) createNode( SUM, "+", val, val2 )
#define SUB_(val, val2) createNode( SUB, "-", val, val2 )
#define MULT_(val, val2) createNode( MULT, "*", val, val2 )
#define DIV_(val, val2) createNode( DIV, "/", val, val2 )
#define NUM_(val) createNode( NUM, "n", nullptr, nullptr, val )
#define VAR_() createNode( VAR, "x" )
#define SIN_(val) createNode( SIN, "sin", nullptr, val )
#define COS_(val) createNode( COS, "cos", nullptr, val )
#define TG_(val) createNode( TG, "tg", nullptr, val )
#define CTG_(val) createNode( CTG, "ctg", nullptr, val )
#define LN_(val) createNode( LN, "ln", nullptr, val )
#define POW_(val, val2) createNode( POW, "pow", val, val2 )
#define SQRT_( val ) POW_( val, NUM_( 0.5 ) )


#define dL diff( workTree -> left, out )
#define dR diff( workTree -> right, out )
#define cL copySubTree( workTree -> left )
#define cR copySubTree( workTree -> right )


const char* str = "";
bool SyntaxError = false;
const char logName[] = "calc.log";
const char separation[] = "\n------------------------------------------------------------------------------------\n";

TreeNode *getG();
TreeNode *getE();
TreeNode *getT();
TreeNode *getP();
TreeNode *getN();


#define ThrowSyntaxError \
	SyntaxError = true;\
	printSyntaxError( (char*)__FILE__, __LINE__, (char*)__func__ );\
	abort();


void LatexHeader ( FILE* out ){
	fprintf (out, "\\documentclass[a4paper,16pt]{article} \n");
    fprintf (out, "\\title {Derivative} \n");
    fprintf (out, "\\begin{document} \n \\maketitle \n \\begin{math} \n");

    return;
}

void handleInput( char *input ){
	int from = 0, to = 0;
	while( input[from] != '\0' ){
		if( input[from] == ' ' || input[from] == '\n' ){
			from++;
		} else {
			input[to++] = input[from++];
		}
	}
	return;
}

void clearLog(){
	FILE *out = fopen( logName, "w" );
	fclose( out );
}

void msgToLog( const char *msg, const char *file, const int line, const char *func ){
	FILE *out = fopen( logName, "at" );
	fprintf( out, "%s in file %s at line %d, func %s%s", msg, file, line, func, separation );
	fclose( out );
}

void printSyntaxError( const char *file, const int line, const char *func ){
	msgToLog( "SYNTAX ERROR", file, line, func );
}


void simplify( TreeNode *cur ){
	if( !cur ) return;
	if( cur -> type == NUM || cur -> type == VAR ) return;

	simplify( cur -> left );
	simplify( cur -> right );

	if( cur -> type == POW ){
		if( cur->right->type == NUM && cur->right->val == 0 ){
			TreeNode *setMeFree1 = cur->left, *setMeFree2 = cur->right;
			*cur = *(cur->right);
			cur->val = 1;
			free( setMeFree1 );
			return;
		}
		if( cur->left->type == NUM && cur->left->val == 1 ){
			TreeNode *setMeFree1 = cur->left, *setMeFree2 = cur->right;
			*cur = *(cur->left);

			free( setMeFree1 );
			free( setMeFree2 );
			return;
		}
		if( cur->right->type == NUM && cur->right->val == 1 ){
			TreeNode *setMeFree1 = cur->left, *setMeFree2 = cur->right;
			*cur = *(cur->left);

			free( setMeFree1 );
			free( setMeFree2 );
			return;
		}
	}
	if( cur -> type == SUM ){
		if( cur->left->type == NUM && cur->right->type == NUM ){
			cur->type = NUM;
			strcpy(cur->data, "NUM");
			cur->val = cur->left->val + cur->right->val;

			free(cur->left);
			cur->left = nullptr;

			free( cur->right);
			cur->right = nullptr;
			return;
		}
		if( cur->left->type == NUM && cur->left->val == 0 ){
			TreeNode *setMeFree1 = cur->left, *setMeFree2 = cur->right;
			*cur = *(cur->right);

			free( setMeFree1 );
			free( setMeFree2 );
			return;
		}
		if( cur->right->type == NUM && cur->right->val == 0 ){
			TreeNode *setMeFree1 = cur->left, *setMeFree2 = cur->right;
			*cur = *(cur->left);

			free( setMeFree1 );
			free( setMeFree2 );

			return;
		}
	}
	if( cur -> type == SUB ){
		if( cur->left->type == NUM && cur->right->type == NUM ){
			cur->type = NUM;
			strcpy(cur->data, "NUM");
			cur->val = cur->left->val - cur->right->val;

			free(cur->left);
			cur->left = nullptr;

			free( cur->right);
			cur->right = nullptr;
			return;
		}
		if( cur->right->type == NUM && cur->right->val == 0 ){
			TreeNode *setMeFree1 = cur->left, *setMeFree2 = cur->right;
			*cur = *(cur->left);

			free( setMeFree1 );
			free( setMeFree2 );

			return;
		}
	}
	if( cur -> type == MULT ){
		if( cur->left->type == NUM && cur->right->type == NUM ){
			cur->type = NUM;
			strcpy(cur->data, "NUM");
			cur->val = cur->left->val * cur->right->val;

			free(cur->left);
			cur->left = nullptr;

			free( cur->right);
			cur->right = nullptr;
			return;
		}
		if( cur->left->type == NUM && cur->left->val == 0 ){
			TreeNode *setMeFree1 = cur->left, *setMeFree2 = cur->right;
			*cur = *(cur->left);

			free( setMeFree1 );
			free( setMeFree2 );
			return;
		}
		if( cur->left->type == NUM && cur->left->val == 1 ){
			TreeNode *setMeFree1 = cur->left, *setMeFree2 = cur->right;
			*cur = *(cur->right);

			free( setMeFree1 );
			free( setMeFree2 );
			return;
		}
		if( cur->right->type == NUM && cur->right->val == 0 ){
			TreeNode *setMeFree1 = cur->left, *setMeFree2 = cur->right;
			*cur = *(cur->right);

			free( setMeFree1 );
			free( setMeFree2 );
			return;
		}
		if( cur->right->type == NUM && cur->right->val == 1 ){
			TreeNode *setMeFree1 = cur->left, *setMeFree2 = cur->right;
			*cur = *(cur->left);

			free( setMeFree1 );
			free( setMeFree2 );
			return;
		}
	}
	if( cur -> type == DIV ){
		if( cur->left->type == NUM && cur->right->type == NUM && cur->right->val != 0 ){
			cur->type = NUM;
			strcpy(cur->data, "NUM");
			cur->val = cur->left->val * cur->right->val;

			free(cur->left);
			cur->left = nullptr;

			free( cur->right);
			cur->right = nullptr;
			return;
		}
		if( cur->left->type == NUM && cur->left->val == 0 ){
			TreeNode *setMeFree1 = cur->left, *setMeFree2 = cur->right;
			*cur = *(cur->left);

			free( setMeFree1 );
			free( setMeFree2 );
			return;
		}
		if( cur->right->type == NUM && cur->right->val == 1 ){
			TreeNode *setMeFree1 = cur->left, *setMeFree2 = cur->right;
			*cur = *(cur->left);

			free( setMeFree1 );
			free( setMeFree2 );
			return;
		}
	}
}


TreeNode *getN(){
	double val = 0;

	if( ( '0' > *str ) || ( *str > '9' ) ){
		ThrowSyntaxError;
	}
	while( ('0' <= *str) && ( *str <= '9') ) {
		val = val * 10 + ( *str - '0' );//семантика
		str++;
	}

	return NUM_(val);
}

TreeNode *getId(){
	return VAR_();
}

TreeNode *getF(){

	if( strncmp( str, "sin", 3 ) == 0 ){
		str += 3;
		return SIN_( getP() );
	}
	if( strncmp( str, "cos", 3 ) == 0 ){
		str += 3;
		return COS_( getP() );
	}
	if( strncmp( str, "tg", 2 ) == 0 ){
		str += 2;
		return TG_( getP() );
	}
	if( strncmp( str, "ctg", 3 ) == 0 ){
		str += 3;
		return CTG_( getP() );
	}
	if( strncmp( str, "ln", 2 ) == 0 ){
		str += 2;
		return LN_( getP() );
	}
	if( strncmp( str, "sqrt", 4 ) == 0 ){
		str += 4;
		return SQRT_( getP() );
	}
	return nullptr;
}

TreeNode *getP(){
	TreeNode *val = {};
	if( *str == '(' ){
		str++;
		val = getE();
		if( *str != ')' ){
			ThrowSyntaxError;
		}
		str++;
	}
	else if( *str == 'x' ){
		str++;
		return VAR_();
	}
	else if( *str >= '0' && *str <= '9'){
		val = getN();
	}
	else {
		val = getF();
	}

	return val;
}

TreeNode *getD(){
	TreeNode *val = getP();
	if( *str == '^' ){
		str++;

		TreeNode *val2 = getP();
		val = POW_( val, val2 );
	}

	return val;
}

TreeNode *getT(){

	TreeNode * val = getD();

	while( *str == '*' || *str == '/' ){

		char op = *str;
		str++;

		TreeNode * val2 = getD();

		if( op == '*' ){
			//val *= val2;
			val = MULT_( val, val2 );
		} else if( op == '/' ){
			//val /= val2;
			val = DIV_( val, val2 );
		}

	}

	return val;
}

TreeNode *getE(){
	TreeNode *val = getT();

	while( *str == '+' || *str == '-' ){

		char op = *str;
		str++;

		TreeNode *val2 = getT();

		if( op == '+' ){
			//val += val2;
			val = SUM_( val, val2 );
		} else if( op == '-' ){
			//val -= val2;
			val = SUB_( val, val2 );
		}
	}

	return val;

}

TreeNode *getG(){
	return getE();

	if( *str != '\0' ){
		ThrowSyntaxError;
	}
}

bool powIsSimple( const TreeNode *cur ){
	if( cur->right->type == NUM ) return true;
	return false;
}

void printTreeLatex( TreeNode *cur, FILE *out ){
	if( !cur ) return;
	//NUM, VAR, SUM, MULT, DIV, SUB, SIN, COS, TG, CTG, LN, POW, SQRT
	if( cur -> type == NUM ){
		fprintf( out, "{%lg}", cur -> val );
		return;
	} else
	if ( cur->type == POW ) {
		fprintf( out, "(" );
		printTreeLatex( cur -> left, out );
		fprintf( out, ")^{" );
		printTreeLatex( cur -> right, out );
		fprintf( out, "}" );
	} else
	if( cur -> type == VAR ){
		fprintf( out, "x" );
		return;
	} else
	if( cur -> type == DIV ){
		fprintf( out, "\\frac " );
		printTreeLatex( cur -> left, out );
		printTreeLatex( cur -> right, out );
		return;
	} else
	if( cur -> type ==  MULT ){
		printTreeLatex( cur -> left, out );
		fprintf( out, "\\cdot " );
		printTreeLatex( cur -> right, out );
		return;
	} else if( cur -> type == SIN ){
		fprintf(out, "\\sin(" );
		printTreeLatex( cur -> right, out );
		fprintf(out, ")" );
	} else if( cur -> type == COS ){
		fprintf(out, "\\cos(" );
		printTreeLatex( cur -> right, out );
		fprintf(out, ")" );
	} else if( cur -> type == TG ){
		fprintf(out, "\\tan(" );
		printTreeLatex( cur -> right, out );
		fprintf(out, ")" );
	} else if( cur -> type == CTG ){
		fprintf(out, "\\cot(" );
		printTreeLatex( cur -> right, out );
		fprintf(out, ")" );
	} else if( cur -> type == LN ){
		fprintf(out, "\\ln(" );
		printTreeLatex( cur -> right, out );
		fprintf(out, ")" );
	} else if( cur -> type == SQRT ){
		fprintf(out, "\\sqrt(" );
		printTreeLatex( cur -> right, out );
		fprintf(out, ")" );
	} else {
		printTreeLatex( cur -> left, out );
		fprintf(out, " %s ", cur -> data );
		printTreeLatex( cur -> right, out );
	}
	return;
}

TreeNode *diff( TreeNode *workTree, FILE *out ){
	//NUM, VAR, SUM, MULT, DIV, SUB, SIN, COS, TAN, ATAN
	TreeNode *ret = nullptr;
	switch( workTree -> type ){
		case NUM:
			ret = NUM_( 0 );
			break;
		case VAR:
			ret = NUM_( 1 );
			break;
		case SUM:
			ret = SUM_( dL, dR );
			break;
		case MULT:
			ret = SUM_( MULT_(dL, cR), MULT_(cL, dR) );
			break;
		case DIV:
			ret = DIV_( SUB_( MULT_(dL, cR), MULT_(cL, dR) ), MULT_(cR, cR) );
			break;
		case SUB:
			ret = SUB_( dL, dR );
			break;
		case SIN:
			ret = MULT_( dR, COS_( cR ) );
			break;
		case COS:
			ret = MULT_( dR, MULT_( NUM_( -1 ), SIN_( cR ) ) );
			break;
		case TG:
			ret = MULT_( dR, DIV_( NUM_( 1 ), MULT_( COS_( cR ), COS_( cR ) ) ) );
			break;
		case CTG:
			ret = MULT_( dR, DIV_( NUM_( -1 ), MULT_( SIN_( cR ), SIN_( cR ) ) ) );
		case LN:
			ret = DIV_( NUM_(1), cR );
		case POW:
			if( powIsSimple( workTree ) ){
				ret = MULT_( dL, MULT_( cR, POW_( cL, SUB_( cR, NUM_(1) ) ) ) );//x`*n*x^(n-1)
			} else {
				ret = MULT_( POW_( cL, cR ), SUM_( MULT_(  DIV_( NUM_(1), cL ), cR ), MULT_( dR, LN_( cL ) ) ) );
			}
			break;
		case SQRT:
			ret = MULT_( dL, MULT_( cR, POW_( NUM_( 0.5 ), NUM_( -0.5 ) ) ) );//x`*n*x^(n-1)
			break;
	}
	fprintf( out, "$(" );
	printTreeLatex( workTree, out );
	fprintf( out, ")' = (");
	printTreeLatex( ret, out );
	fprintf (out, ")$\\newline\n");
	return ret;
}

int main(){
	clearLog();

	char input[1000] = "";
	scanf( "%s", input );
	scanf( "%*c" );

	handleInput( input );

	// printf("%s\n", input);

	int syntaxError = 0;


	str = input;

	TreeNode *tree = getG();
	showTree( tree );

	simplify( tree );

	FILE *latex = fopen( latexFileName, "w" );
	LatexHeader( latex );

	TreeNode *d = diff( tree, latex );
	showTree( d );

	simplify( d );
	showTree( d );

	fprintf( latex, "Final:\n\n" );
	printTreeLatex( d, latex );

	fprintf ( latex, "\n \\end{math} \n \\end{document}\n");
	fclose( latex );
	system ("pdflatex diff.tex");
	system ("xdg-open diff.pdf");

	return 0;
}
