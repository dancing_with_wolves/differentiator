#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "tree.h"

const char *errMsg[]{
	"OK", "EMPTY", "NEGATIVE_SIZE", "EXTERNAL_INTRUSION", "RELATIONSHIP_ERROR", "WRONG_CHILDREN_QTY"
};

int countChildrenQty( TreeNode *this_ ){
	if( !this_-> left && !this_-> right ){
		return 1;
	}
	return countChildrenQty( this_-> left ) + countChildrenQty( this_-> right );
}

void msgToLog( const char *msg ){
	FILE *out = fopen ( logName, "at" );
	fprintf( out, "%s", msg );
	fprintf( out, "%s", separation );
	fclose( out );
}

void findNodeIter( TreeNode *cur, Elem_t val, TreeNode* *ans ){
	assert( cur );
	if ( *ans != nullptr ) return;
	if( strcmp( cur -> val, val ) == 0 ) {
		*ans = cur;
		return;
	}

	if( cur -> left != nullptr ) findNodeIter( cur -> left, val, ans );
	if ( *ans != nullptr ) return;
	if( cur -> right != nullptr ) findNodeIter( cur -> right, val, ans );

}

TreeNode *findNode( Tree *this_, Elem_t val ){
	assert( this_ );
	TreeNode *ans = nullptr;
	findNodeIter ( this_-> root, val, &ans );
	return ans;
}

char readBuf( FILE *in, char *buf, char curSym ){
	int i = 0;

	while( curSym != '{' && curSym != ',' && curSym != '}') {
		buf[i++] = curSym;
		curSym = fgetc( in );
	}
	buf[i] = '\0';
	return curSym;
}

TreeNode *readTreeNode( FILE *in, TreeNode *parent ){

	TreeNode *newNode = ( TreeNode* ) calloc ( 1, sizeof( TreeNode ) );
	newNode -> left = nullptr;
	newNode -> right = nullptr;
	newNode -> parent = parent;

	char curSym = fgetc( in );
	if( curSym == '}' || curSym == ',' ){
		msgToLog( "Incorrect input format");
		return nullptr;
	}

	if( curSym == '{' ){// считываем лист

		//считываем строку в буфер
		char buf[128] = "";
		int i = 0;

		curSym = fgetc( in );
		curSym = readBuf( in, buf, curSym );


		if( curSym == ',' || curSym == '{' ){
			msgToLog( "Incorrect input format");
			return nullptr;
		}
		newNode -> val = strdup( buf );

	} else if( curSym != '{' ){ //считываем промежуточную ноду с вопросом

		//считываем строку в буфер
		char buf[128] = "";
		int i = 0;

		curSym = readBuf( in, buf, curSym );

		if( curSym == ',' || curSym == '}' ){
			msgToLog( "Incorrect input format");
			return nullptr;
		}

		newNode -> val = strdup( buf );

		newNode -> left = readTreeNode( in, newNode );//рекурсивный вызов

		//если нужно прокинуть эксепшн
		if( newNode -> left == nullptr ){
			return nullptr;
		}

		if( fgetc( in ) != ',' ) {
			msgToLog( "Incorrect input format");
			return nullptr;
		}

		newNode -> right = readTreeNode( in, newNode );//рекурсивный вызов

		//если нужно прокинуть эксепшн
		if( newNode -> left == nullptr ){
			return nullptr;
		}
		if( fgetc( in ) != '}' ) {
			msgToLog( "Incorrect input format");
			return nullptr;
		}
	}

	return newNode;
}

void skipSymbols( FILE *in ){
	assert( in );
	char cur = fgetc( in );
	while( cur == ' ' || cur == '\n' ){
		cur = fgetc( in );
	}
	fseek( in, -1, SEEK_CUR );
}

void writeTreeNode( TreeNode *cur, FILE *out ){
	if( cur == nullptr ) {
		fprintf( out, "{$}" );;
		return;
	}
	if( ( cur -> left == nullptr ) && ( cur -> right == nullptr ) ){
		fprintf( out, "{%s}", cur -> val );
		return;
	} else {
		fprintf( out, "%s", cur -> val );
	}
	fprintf( out, "{" );
	writeTreeNode( cur -> left, out );
	fprintf( out, ",");
	writeTreeNode( cur -> right, out );
	fprintf( out, "}" );
}

void writeTree( char *fname, Tree *workTree ){
	FILE *out = fopen( fname, "w" );
	writeTreeNode( workTree -> root, out );
	fclose( out );
}

void readTree( char *fname, Tree *workTree ){
	assert( fname );
	assert( workTree );
	FILE *in = fopen( "data.tree", "r" );

	TreeNode *curNode = workTree -> root;

	skipSymbols( in );
	workTree -> root = readTreeNode( in, workTree -> root );

	fclose( in );
}

bool checkTreeRelationshipsNode( TreeNode *cur ){
	if( !cur ) return true;
	if( !cur -> left && !cur -> right ){
		return true;
	}
	if( cur -> left ){
		if( cur != cur -> left -> parent ){
			return false;
		}
		return checkTreeRelationshipsNode( cur -> left );
	}
	if( cur -> right ){
		if( cur != cur -> right -> parent ){
			return false;
		}
		return checkTreeRelationshipsNode( cur -> right );
	}
}

bool checkTreeRelationships( Tree *this_ ){
	return checkTreeRelationshipsNode( this_-> root );
}

int countTreeVert( Tree *this_ ){
	return this_-> vertQty;
}

treeErr treeOk( Tree *this_ ){
	if( this_-> vertQty < 0 ){
		this_-> err = NEGATIVE_SIZE;
		return NEGATIVE_SIZE;
	}
	if( !checkTreeRelationships( this_ ) ){
		this_-> err = RELATIONSHIP_ERROR;
		return RELATIONSHIP_ERROR;
	}
	if( countTreeVert( this_ ) != this_-> vertQty ){
		this_-> err = WRONG_CHILDREN_QTY;
		return WRONG_CHILDREN_QTY;
	}

	this_-> err = OK;
	return OK;
}

void treeInit( Tree *this_ ){
	this_ -> root = nullptr;
	this_ -> vertQty = 0;
	assert( treeOk( this_ ) == OK );
}

void clearLog(){
    FILE *out = fopen ( logName, "w" );
    fclose( out );
}

TreeNode *createNode( TreeNode *parent, TreeNode *left, TreeNode *right, Elem_t value ){
	TreeNode *this_ = ( TreeNode* ) calloc ( 1, sizeof( TreeNode ) );
	this_-> val = value;
	this_-> parent = parent;
	this_-> left = left;
	this_-> right = right;
	return this_;
}

void descructTreeRec( TreeNode *cur ){
	if( cur -> left ){
		descructTreeRec( cur -> left );
	}
	if( cur -> right ){
		descructTreeRec( cur -> left );
	}
	free( cur );
}

void destructTree( Tree *this_ ){
	descructTreeRec( this_ -> root );
	*this_ = {};
}

void printTreeIter( const TreeNode *cur, printOrder order ){
	if( order == -1 ){
		printf( "%s\n", cur -> val );
		if( cur -> left ){
			printTreeIter( cur -> left, order );
		}
		if( cur -> right ){
			printTreeIter( cur -> right, order );
		}
	}
	if( order == 0 ){
		if( cur -> left ){
			printTreeIter( cur -> left, order );
		}
		printf( "%s\n", cur -> val );
		if( cur -> right ){
			printTreeIter( cur -> right, order );
		}
	}
	if( order == 1 ){
		if( cur -> left ){
			printTreeIter( cur -> left, order );
		}
		if( cur -> right ){
			printTreeIter( cur -> right, order );
		}
		printf( "%s\n", cur -> val );
	}
}

void printTree( Tree *this_, printOrder order ){
	printTreeIter( this_ -> root, order );
	assert( treeOk( this_ ) == OK );
}

void addChild( Tree* this_, Elem_t val, TreeNode *parent, childSide side ){
	assert( treeOk ( this_ ) == OK );

	TreeNode *newNode = createNode( parent, nullptr, nullptr, val );

	if( this_-> root == nullptr ){
		if( parent == nullptr ){
			this_-> root = newNode;
			this_-> vertQty++;
			msgToLog( "Initialised new tree!");
			assert( treeOk( this_ ) == OK );
			return;
		} else {
			msgToLog( "Cannot add non-null parent's child to empty tree!");
			return;
		}
	}
	if( parent == nullptr ){
		msgToLog( "Cannot add null parent's child!");
	}

	if( side == leftSide ){
		parent -> left = newNode;
	} else {
		parent -> right = newNode;
	}
	this_ -> vertQty++;
	assert( treeOk( this_ ) == OK );
}

void addSubTree( Tree *parentTree, TreeNode *parent, Tree *subTree, childSide side ){
	assert( treeOk( parentTree ) == OK );
	assert( treeOk( subTree ) == OK );

	parentTree -> vertQty += subTree -> vertQty;
	if( side == leftSide ){
		parent -> left = subTree -> root;
	} else {
		parent -> right = subTree -> root;
	}
	destructTree( subTree );
	assert( treeOk( parentTree ) == OK );
}

void deleteChild( Tree *this_, TreeNode *child ){

	assert( treeOk( this_ ) == OK );

	TreeNode *parent = child -> parent;

	int side = -1;
	if( parent -> left == child ){
		parent -> left = nullptr;
		side = leftSide;
	} else if ( parent -> right == child ){
		parent -> right = nullptr;
		side = rightSide;
	}

	if( child -> left || child -> right || side == -1 ){
		msgToLog( "Cannot delete a child!\n" );
		return;
	}

	free( child -> val );
	this_ -> vertQty--;
	assert( treeOk( this_ ) == OK );
}

Tree *initSubTree( TreeNode *root ){
	Tree *tree = ( Tree* ) calloc ( 1, sizeof( Tree ) );
	tree -> root = root;
	tree -> vertQty = countChildrenQty( root );
	assert( treeOk( tree ) == OK );
	return tree;
}

void deleteSubTree( Tree *this_, Tree *subTree, TreeNode *node ){
	assert( treeOk( this_ ) == OK );

	if( node ){
		Tree *tree = initSubTree( node );
		deleteSubTree( this_, tree );
		return;
	}

	assert( treeOk( subTree ) == OK );
	this_-> vertQty -= subTree-> vertQty;

	TreeNode *parent = subTree -> root -> parent;

	if( parent -> left == subTree -> root ){
		parent -> left = nullptr;
	} else if ( parent -> right == subTree -> root ){
		parent -> right = nullptr;
	}

	destructTree( subTree );
	assert( treeOk( this_ ) == OK );
}

void tellMeEverythingYouKnow( Tree *this_ ){

}
