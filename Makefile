run: build
	./calc.e
build: calc.cpp
	clang++ calc.cpp -o calc.e
clean:
	rm -f *.log
	rm -f *.pdf
	rm -f *.tex
	rm -f *.gpvz
	rm -f *.png
	rm -f *.e
	rm -f *.aux

