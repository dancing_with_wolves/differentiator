#include <stdio.h>
#include <stdlib.h>

const int MAX_DATA_LENGTH = 16;

enum nodeType{
	NUM, VAR, SUM, MULT, DIV, SUB, SIN, COS, TG, CTG, LN, POW, SQRT
};

const char *nodeTypeStrings[] = {
	"NUM", "VAR", "SUM", "MULT", "DIV", "SUB", "SIN", "COS", "TG", "CTG", "LN", "POW", "SQRT"
};

struct TreeNode{
	nodeType type;
	char data[ MAX_DATA_LENGTH ];
	double val;
	TreeNode *left, *right;
};

void showTreeIter( TreeNode *cur, FILE *out ){
	if( !cur ) return;
	assert( out );

	if( cur -> type != NUM ){
		fprintf( out, "%d [ label = \"%s\" ]\n", cur, cur -> data );
	} else {
		fprintf( out, "%d [ label = \"%lg\" ]\n", cur, cur -> val );
	}
	if( cur -> left ) fprintf( out, "%d -> %d\n", cur, (cur -> left) );
	if( cur -> right ) fprintf( out, "%d -> %d\n", cur, (cur -> right) );

	showTreeIter( cur -> left, out );

	showTreeIter( cur -> right, out );

}

void showTree( TreeNode *tree ){
	FILE *out = fopen( "tree.gpvz", "w" );
	fprintf( out, "digraph {\n" );

	showTreeIter( tree, out );

	fprintf( out, "}\n" );
	fclose( out );
	system( "dot tree.gpvz -T png -o tree_image.png" );
	system( "eog tree_image.png" );
}


TreeNode *copySubTree( TreeNode *from ){
	if( !from ) return nullptr;

	TreeNode *ret = ( TreeNode* ) calloc ( 1, sizeof( TreeNode ) );
	ret -> type = from -> type;
	ret -> val = from -> val;
	strcpy( ret -> data, from -> data );

	ret -> left = copySubTree( from -> left );
	ret -> right = copySubTree( from -> right );

	return ret;
}

TreeNode *createNode( const nodeType type, const char *data, TreeNode *left = nullptr, TreeNode *right = nullptr, double val = -10. ){
	assert( data );

	TreeNode *new_ = (TreeNode*) calloc ( 1, sizeof( TreeNode ) );

	new_-> type = type;
	strcpy( new_-> data, data );
	new_-> val = val;
	new_-> left = left;
	new_-> right = right;
	return new_;
}


/*#include <stdio.h>

typedef char* Elem_t;

const char separation[] = "\n------------------------------------------------------------------------------------\n";
const char logName[] = "treeLog.log";

enum treeErr {
    OK = 0, EMPTY = 1, NEGATIVE_SIZE = 2, EXTERNAL_INTRUSION = 3, RELATIONSHIP_ERROR = 4, WRONG_CHILDREN_QTY = 5
};

enum printOrder{
	preOrder = -1, inOrder = 0, postOrder = 1
};

enum childSide {
	leftSide = 0, rightSide = 1
};

extern const char *errMsg[];

struct TreeNode{
	Elem_t val;
	TreeNode *parent;
	TreeNode *left;
	TreeNode *right;
};

struct Tree{
	TreeNode *root;
	treeErr err;
	int vertQty;
};

TreeNode *findNode( Tree *this_, Elem_t val );

void clearLog();

void msgToLog( const char *msg );

void treeInit( Tree *tree );

void destructTree( Tree *tree );

treeErr treeOk( Tree *tree );

void printTree( const Tree *tree, printOrder order = preOrder );

void addChild( Tree* this_, Elem_t val, TreeNode *parent, childSide side = leftSide );

void addSubTree( Tree *parentTree, TreeNode *parent, Tree *subTree, childSide side = leftSide );

void deleteChild( Tree *this_, TreeNode *child );

void deleteSubTree( Tree *this_, Tree *subTree = nullptr, TreeNode *node = nullptr );

void tellMeEverythingYouKnow( Tree *tree );

void showTree( Tree *this_ );

void writeTree( char *fname, Tree *workTree );

void readTree( char *fname, Tree *workTree );*/
